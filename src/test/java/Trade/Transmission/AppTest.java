package Trade.Transmission;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     * @throws ParseException 
     */
    public void testApp() throws ParseException
    {
        List<Trades> trades = new LinkedList<Trades>();
        trades.add(new Trades(1, 1, "CP-1", "B1", new SimpleDateFormat("dd/MM/yyyy").parse("20/05/2021"),new Date(), false));
        assertEquals(true, App.loadAllTrades(trades));
        assertEquals(1, App.lowestVersion(trades));
        assertEquals(true, App.updateVersionMatchingRecord(trades, 1, new SimpleDateFormat("dd/MM/yyyy").parse("21/05/2021")));
        assertEquals(false, App.updateVersionMatchingRecord(trades, 6, new SimpleDateFormat("dd/MM/yyyy").parse("21/05/2021")));
    }
}
