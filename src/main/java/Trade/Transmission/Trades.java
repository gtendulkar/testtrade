/**
 * 
 */
package Trade.Transmission;

import java.util.Date;
import java.util.List;

/**
 * @author GTendulkar
 *
 */
public class Trades {
	
	int tradeId;
	int version;
	String counterPartyId;
	String bookId;
	Date maturity;
	Date created;
	boolean expired;
	
	
	
	
	public Trades(int tradeId, int version, String counterPartyId, String bookId, Date maturity, Date created,
			boolean expired) {
		super();
		this.tradeId = tradeId;
		this.version = version;
		this.counterPartyId = counterPartyId;
		this.bookId = bookId;
		this.maturity = maturity;
		this.created = created;
		this.expired = expired;
	}




	/**
	 * @return the tradeId
	 */
	public int getTradeId() {
		return tradeId;
	}




	/**
	 * @param tradeId the tradeId to set
	 */
	public void setTradeId(int tradeId) {
		this.tradeId = tradeId;
	}




	/**
	 * @return the version
	 */
	public int getVersion() {
		return version;
	}




	/**
	 * @param version the version to set
	 */
	public void setVersion(int version) {
		this.version = version;
	}




	/**
	 * @return the counterPartyId
	 */
	public String getCounterPartyId() {
		return counterPartyId;
	}




	/**
	 * @param counterPartyId the counterPartyId to set
	 */
	public void setCounterPartyId(String counterPartyId) {
		this.counterPartyId = counterPartyId;
	}




	/**
	 * @return the bookId
	 */
	public String getBookId() {
		return bookId;
	}




	/**
	 * @param bookId the bookId to set
	 */
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}




	/**
	 * @return the maturity
	 */
	public Date getMaturity() {
		return maturity;
	}




	/**
	 * @param maturity the maturity to set
	 */
	public void setMaturity(Date maturity) {
		this.maturity = maturity;
	}




	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}




	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}




	/**
	 * @return the expired
	 */
	public boolean isExpired() {
		return expired;
	}




	/**
	 * @param expired the expired to set
	 */
	public void setExpired(boolean expired) {
		this.expired = expired;
	}
	
	

}
