package Trade.Transmission;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;


public class App 
{
	public static void main( String[] args )
    {
    	List<Trades> trades = new LinkedList<Trades>();
    	
    	loadAllTrades(trades);
    	
    	System.out.println("*************************Total Trades********************************************");
    	
    	getAllTrades(trades);
    	
    	System.out.println("*************************Begin Transmission **************************************");
    	//Get inputs from user
    	trades = getInputs(trades);
    	System.out.println("*************************End Transmission **************************************");
    	
    	checkforAllExpiredTrades(trades);
    	
    	System.out.println("*************************Total New Trades********************************************");
    	
    	getAllTrades(trades);
    	
   }
	
	public static void getAllTrades(List<Trades> trades)
	{
		System.out.println("TRADE ID | VERSION | COUNTER PARTY ID | BOOK ID | MATURITY DATE | CREATED DATE | EXPIRED");
    	
    	for(Trades trade : trades)
    	{
    		System.out.println(trade.getTradeId() + "  | " + trade.getVersion() + " | " + trade.getCounterPartyId() + " | " + trade.getBookId() + " | " + trade.getMaturity() + " | " 
    	+ trade.getCreated() + " | " +trade.isExpired());
    	}
	}
    
    public static boolean loadAllTrades(List<Trades> trades)
    {
    	boolean loaded = false;
		try
		{
			trades.add(new Trades(1, 1, "CP-1", "B1", new SimpleDateFormat("dd/MM/yyyy").parse("20/05/2021"),new Date(), false));
			trades.add(new Trades(2, 2, "CP-2", "B1", new SimpleDateFormat("dd/MM/yyyy").parse("20/05/2021"),new Date(), false));
			trades.add(new Trades(3, 1, "CP-1", "B1", new SimpleDateFormat("dd/MM/yyyy").parse("20/05/2021"),new Date("14/03/2015"), false));
			trades.add(new Trades(4, 3, "CP-3", "B2", new SimpleDateFormat("dd/MM/yyyy").parse("20/05/2014"),new Date(),true));
			loaded = true;
		}
		catch(Exception e)
		{
			System.out.println("Error occurred while loading all the trades : "+e);
		}
		return loaded;
    }
    
    public static List<Trades> getInputs(List<Trades> trades)
    {
    	try
    	{
    		Scanner input = new Scanner(System.in);
        	
        	System.out.print("\nEnter the number of trades you would like to make : ");
        	
        	int tradeCount = input.nextInt();
        	
        	int existingTradeCount = trades.size();
        	
        	for(int i =0;i<tradeCount;i++)
        	{
        		System.out.print("\nEnter the details of trades you would like to enter : ");
        		
        		System.out.println("\n Version [Should be integer number] :");
        		
        		int version = input.nextInt();
        		
        		while(version < lowestVersion(trades))
        		{
        			System.out.println("\n Please enter Version [Should be integer number] with a greater number :");
        			
        			version = input.nextInt();
        		}
        		
        		System.out.println("\n Maturity date [should be in format dd/mm/yyyy]");
        		
        		Date maturityDate =new SimpleDateFormat("dd/MM/yyyy").parse(input.next());
        		
        		//Validation
        		
        		while(maturityDate.before(new Date()))
        		{
        			System.out.println("\n Please enter Maturity date [should be in format dd/mm/yyyy] greater than todays date :");
        			
        			maturityDate = new SimpleDateFormat("dd/MM/yyyy").parse(input.next());
        			
        		}
        		boolean updated = updateVersionMatchingRecord(trades, version, maturityDate);
        		if(!updated)
        		{
        			trades.add(new Trades(existingTradeCount, version, "CP-1", "B-2", maturityDate, new Date(), false));
        			existingTradeCount++;
        		}
        	}
    	}
    	catch(Exception e)
    	{
    		System.out.println("Error occurred while entering data : "+e);
    	}
    	return trades;
    	
    }
    
    public static int lowestVersion( List<Trades> trades)
    {
    	int min = trades.get(0).getVersion();
    	
    	 for (int i = 1; i < trades.size(); i++) {
             if (trades.get(i).getVersion() < min) {
                 min = trades.get(i).getVersion();
             }
         }
    	 
    	 return min;
    }
    
    public static boolean updateVersionMatchingRecord(List<Trades> trades,int version,Date maturityDate)
    {
    	boolean updated = false;
    	
    	if(trades.size() > 0)
    	{
    		for(Trades trade : trades)
        	{
        		try
            	{
            		if(version==trade.getVersion())
            		{
            			trade.setMaturity(maturityDate);
            			updated = true;
            		}
            	}
        		catch(Exception e)
        		{
        			System.out.println("Error while updating record : "+ e);
        		}
        	}
    	}
    	
    	return updated;
    	
    }
    
    public static void checkforAllExpiredTrades(List<Trades> trades)
    {
    	if(trades.size() > 0)
    	{
    		for(Trades trade : trades)
        	{
        		try
            	{
            		if(trade.getMaturity().before(trade.created))
            		{
            			trade.setExpired(true);
            		}
            	}
        		catch(Exception e)
        		{
        			System.out.println("Error while updating record : "+ e);
        		}
        	}
    	}
    }
}
